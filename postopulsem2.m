function [Endpoint_lfull,Endpoint_l,Endpoint_Highqq] = postopulsem2(data_p,data_v)
format long
b = 3022.5; a = 3022.5; % a v? b l? chi?u r?ng v? chi?u d?i, c l? cao t?nh t? m?t d?t l?n n?i g? pulley
b1 = 45; a1 = 275; c1 = 67;
aa = 475; bb = 450; cc1 = 1400; cc2 =0; % k?ch th??c khung m?i 1460 l? chi?u cao
r=15.5; pi=3.1415926535897932384626433; R=32;
PB = [-aa  aa -aa  aa -aa  aa -aa  aa;
       bb -bb -bb  bb -bb  bb  bb -bb;
      cc2 cc1 cc2 cc1 cc1 cc2 cc1 cc2];
% PB = [-192  180 -175  185 -205  175 -180  190;
%        185 -190 -185  180 -180  185  192 -175;
%       cc2 cc1 cc2 cc1 cc1 cc2 cc1 cc2];
OG = [b1-a a1-a a-a1 a-b1 b1-a a1-a a-a1 a-b1;
      a1-b b1-b b1-b a1-b b-a1 b-b1 b-b1 b-a1;
      3449    3449    3460    3460    3447    3447    3500    3500];
OB = zeros(3,8);
l_BJ = zeros(1,8);
l_BA = zeros(1,8);
l_full = zeros(1,8);
BG = zeros(3,8);
BA = zeros(3,8);
AJ = zeros(3,8);
u = zeros(3,8);
bxu = zeros(6,8);
bxu2 = zeros(8,6);
OJ = zeros(3,8);
OA = zeros(3,8);
BJ = zeros(3,8);
row = 1;
Endpoint_l = zeros(row,8);
Endpoint_lfull = zeros(row,8);
Endpoint_t = zeros(row,8);

lo = 1.0e+03*[5.295004948197559 4.552469925045038 5.328935594764962 4.525905422257469 4.520115627592081 5.320583558400154 4.575215407145810 5.328055765172058];

for t=1:row
    t_x = data_p(t,1);
    t_y = data_p(t,2);
    t_z = data_p(t,3);
%         t_x = 0;
%         t_y = 0;
%         t_z = 0;
%     gan gia tri cho cac vector
    OP = [t_x;t_y;t_z];
    data_px = data_p(t,4:6)*pi/180;
    sax = sin(data_px(1));
    say = sin(data_px(2));
    saz = sin(data_px(3));

    cax = sqrt(1-sax^2);
    cay = sqrt(1-say^2);
    caz = sqrt(1-saz^2);

    Rz = [caz -saz 0;
          saz  caz 0;
          0    0   1];
    Ry = [cay  0 say;
          0    1   0;
          -say 0 cay];
    Rx = [1 0      0;
          0 cax -sax;
          0 sax  cax];
    Rxyz = Rz*Ry*Rx;
    
    for i=1:1:8
        OB(1:3,i) = OP(1:3) + Rxyz*PB(1:3,i);
        BG(1:3,i) = OG(1:3,i) - OB(1:3,i);
    end
    %tinh toan cac vector khac, ket qua duoc lenghtF
    for i=1:1:8
        tan_theta = (OB(2,i)-OG(2,i))/(OB(1,i)-OG(1,i));
        ct = sqrt(1/(1+(tan_theta)^2));
        st = sqrt(1 - (ct)^2);
        temp1 = -sign(BG(1,i));
        temp2 = -sign(BG(2,i));
        OJ(1,i) = temp1*R*ct + OG(1,i);
        OJ(2,i) = temp2*R*st + OG(2,i);
        OJ(3,i) = -c1 + OG(3,i);
        BJ(1:3,i) = OJ(1:3,i) - OB(1:3,i);
        l_BJ(i) = norm(BJ(:,i));
        beta = 2*pi - pi/2 - acos(BJ(3,i)/l_BJ(i)) - acos(r/l_BJ(i));
        OA(1,i) = temp1*r*cos(pi-beta)*ct + OJ(1,i);
        OA(2,i) = temp2*r*cos(pi-beta)*st + OJ(2,i);
        OA(3,i) = r*sin(pi-beta) + OJ(3,i);
        BA(1:3,i) = OA(1:3,i) - OB(1:3,i);
        AJ(1:3,i) = OJ(1:3,i) - OA(1:3,i);
        l_BA(i) = norm(BA(:,i));
        l_full(i) = r*beta + l_BA(i);
        u(1:3,i) = BA(:,i)/l_BA(i);
        bxu(1,i) = u(1,i);
        bxu(2,i) = u(2,i);
        bxu(3,i) = u(3,i);
        bxu(4:6,i) = cross(Rxyz*PB(1:3,i),u(:,i));
        bxu2(i,1) = u(1,i);
        bxu2(i,2) = u(2,i);
        bxu2(i,3) = u(3,i);
        bxu2(i,4:6) = cross(Rxyz*PB(1:3,i),u(:,i)).';
    end
    %Endpoint_l
    Endpoint_l(t,:) = round((l_full-lo)/pi/100*38400);
    Endpoint_lfull(t,:) = l_full; 
    %Endpoint_t
    W = bxu;
    mp = 85;
    I = eye(3);
    MSP = Rxyz*mp*[0;0;180];
    skew_MSP = [0 -MSP(3) MSP(2); MSP(3) 0 -MSP(1); -MSP(2) MSP(1) 0];
    g = [0;0;9800];
    wg = [mp*I;skew_MSP]*g;                                                    
    torque = pinv(W)*wg/1e3;
    Endpoint_t(t,:) = torque.';
    %Endpoint-Lowqq-Highqq-AccDecc
    tempxxx = [1 0 say;0 cax -cay*sax;0 sax cax*cay];
%     tempxxx = 1;
    omega = tempxxx * data_v(4:6).';
    velocity = data_v(1:3).';
    tw = [velocity; omega];
    J = bxu2;
    qq = J*tw*30/50*1280/2/pi;
    max_qq = max(abs(qq));
    Endpoint_Highqq = max_qq;
end
% for i = 1:8
%    a = set_qqq(i);
%    for t = 0:0.02:0.2
%       step = (t/0.02)+1;
%       v(step) = set_qq(i) + a*t;
%       s(step) = set_qq(i)*t + a*t^2/2;
%    end
%    step_t = step;
%    s_t = s(step_t);
%    v_t = v(step_t);
%    for t = 0.22:0.02:0.8
%       step = round(step_t + ((t-0.22)/0.02)+1);
%       v(step) = v_t;
%       s(step) = s_t + v_t*(t-0.22);
%    end
%    step_t2 = step;
%    s_t = s(step_t2);
%    for t = 0.82:0.02:1
%       step = step_t2 + ((t-0.82)/0.02)+1;
%       v(step) = v_t - a*(t-0.82);
%       s(step) = s_t + v_t * t - a*(t-0.82)^2/2;
%    end
%    line(v,s,'Color','magenta');
%    hold on;
% end
end