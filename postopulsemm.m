function [Endpoint_l] = postopulsemm(data_p,data_a)
format long
b = 3022.5; a = 3022.5;
b1 = 45; a1 = 275; c1 = 67;
aa = 475; bb = 450; cc1 = 1400; cc2 =0;
r=15.5; pi=3.1415926535897932384626433; R=32;

PB = [-aa  aa -aa  aa -aa  aa -aa  aa;
       bb -bb -bb  bb -bb  bb  bb -bb;
      cc2 cc1 cc2 cc1 cc1 cc2 cc1 cc2];
% PB = [-192  180 -175  185 -205  175 -180  190;
%        185 -190 -185  180 -180  185  192 -175;
%       cc2 cc1 cc2 cc1 cc1 cc2 cc1 cc2];
OG = [b1-a a1-a a-a1 a-b1 b1-a a1-a a-a1 a-b1;
      a1-b b1-b b1-b a1-b b-a1 b-b1 b-b1 b-a1;
      3449    3449    3460    3460    3447    3447    3500    3500];
OB = zeros(3,8);
l_BJ = zeros(1,8);
l_BA = zeros(1,8);
l_full = zeros(1,8);
BG = zeros(3,8);
BA = zeros(3,8);
AJ = zeros(3,8);
u = zeros(3,8);
bxu = zeros(6,8);
OJ = zeros(3,8);
OA = zeros(3,8);
BJ = zeros(3,8);
lo = 1.0e+03*[5.295004948197559 4.552469925045038 5.328935594764962 4.525905422257469 4.520115627592081 5.320583558400154 4.575215407145810 5.328055765172058];
% lo = 1.0e+03*[5.282954542313818 5.055855767605433 5.288238363392344 5.065255735014447 5.046419461872661 5.279821517115926 5.085745474024447 5.311637271580375];
t_x = data_p(1);
t_y = data_p(2);
t_z = data_p(3);
%         t_x = 0;
%         t_y = 0;
%         t_z = 0;
%     gan gia tri cho cac vector
OP = [t_x;t_y;t_z];
data_a = data_a*pi/180;
sax = sin(data_a(1));
say = sin(data_a(2));
saz = sin(data_a(3));

cax = sqrt(1-sax^2);
cay = sqrt(1-say^2);
caz = sqrt(1-saz^2);

Rz = [caz -saz 0;
      saz  caz 0;
      0    0   1];
Ry = [cay  0 say;
      0    1   0;
      -say 0 cay];
Rx = [1 0      0;
      0 cax -sax;
      0 sax  cax];
Rxyz = Rz*Ry*Rx;

for i=1:1:8
    OB(1:3,i) = OP(1:3) + Rxyz*PB(1:3,i);
    BG(1:3,i) = OG(1:3,i) - OB(1:3,i);
end
%tinh toan cac vector khac, ket qua duoc lenghtF
for i=1:1:8
    tan_theta = (OB(2,i)-OG(2,i))/(OB(1,i)-OG(1,i));
    ct = sqrt(1/(1+(tan_theta)^2));
    st = sqrt(1 - (ct)^2);
    temp1 = -sign(BG(1,i));
    temp2 = -sign(BG(2,i));
    OJ(1,i) = temp1*R*ct + OG(1,i);
    OJ(2,i) = temp2*R*st + OG(2,i);
    OJ(3,i) = -c1 + OG(3,i);
    BJ(1:3,i) = OJ(1:3,i) - OB(1:3,i);
    l_BJ(i) = norm(BJ(:,i));
    beta = 2*pi - pi/2 - acos(BJ(3,i)/l_BJ(i)) - acos(r/l_BJ(i));
    OA(1,i) = temp1*r*cos(pi-beta)*ct + OJ(1,i);
    OA(2,i) = temp2*r*cos(pi-beta)*st + OJ(2,i);
    OA(3,i) = r*sin(pi-beta) + OJ(3,i);
    BA(1:3,i) = OA(1:3,i) - OB(1:3,i);
    AJ(1:3,i) = OJ(1:3,i) - OA(1:3,i);
    l_BA(i) = norm(BA(:,i));
    l_full(i) = r*beta + l_BA(i);
    disp(l_full(i))
    disp(lo(i))
    u(1:3,i) = BA(:,i)/l_BA(i);
    bxu(1,i) = u(1,i);
    bxu(2,i) = u(2,i);
    bxu(3,i) = u(3,i);
    bxu(4:6,i) = cross(Rxyz*PB(1:3,i),u(:,i));
end
%%disp(l_full)
%%disp(lo)
Endpoint_l = round((l_full-lo)/pi/100*38400);
end

